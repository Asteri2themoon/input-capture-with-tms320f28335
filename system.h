/*
 * system.h
 *
 *  Created on: 17 d�c. 2018
 *      Author: aklipfel
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#define CRYSTAL_FREQUENCY ((unsigned long)30e6l)

void init();
void delay(unsigned long dt);//delay in ms (max ~28s)
unsigned long getSystemFrequency();

#endif /* SYSTEM_H_ */
