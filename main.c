#include "config.h"
#include "system.h"

/**
 * main.c
 */

void initCAP();
interrupt void cap1_isr();

#define PERIOD_COUNT 128
volatile unsigned long period[PERIOD_COUNT];
volatile unsigned long intCount;

void main(void)
{
    init();

    //gpio config
    EALLOW;
    GPAPUD.GPIO9=1;
    GPAMUX1.GPIO9=0;
    GPADIR.GPIO9=1;
    GPAPUD.GPIO11=1;
    GPAMUX1.GPIO11=0;
    GPADIR.GPIO11=1;
    EDIS;
    GPADAT.GPIO9=0;
    GPADAT.GPIO11=0;

    intCount=0;

    initCAP();

    //blink (to see if capture is running)
    while(intCount<PERIOD_COUNT){
        GPASET.GPIO9=1;
        delay(500);
        GPACLEAR.GPIO9=1;
        GPASET.GPIO11=1;
        delay(500);
        GPACLEAR.GPIO11=1;
    }
    for(;;);
}

void initCAP(){
    //input capture config
    EALLOW;
    GPACTRL.QUALPRD3=0x00;//sampling period = 1
    GPAMUX2.GPIO24=0x1;//GPIO 24 to input capture (CAP) 1
    GPAMUX2.GPIO26=0x1;//GPIO 26 to input capture (CAP) 3
    PCLKCR1.ECAP1ENCLK=1;//enable cap 1 clock
    EDIS;

    ECAP1.ECCTL1.CAP1POL=0;//event 1 at rising edge
    ECAP1.ECCTL1.CTRRST1=0;//do not reset
    ECAP1.ECCTL1.CAP2POL=0;//event 2 at falling edge
    ECAP1.ECCTL1.CTRRST2=0;//reset counter
    ECAP1.ECCTL1.CAPLDEN=1;//enable capture
    ECAP1.ECCTL1.PRESCALE=0;//divide by 1

    ECAP1.ECCTL2.CAP_APWM=0;//capture mode
    ECAP1.ECCTL2.SWSYNC=1;//synchronizing
    ECAP1.ECCTL2.STOP_WRAP=0x1;//stop at event 2
    ECAP1.ECCTL2.CONT_ONESH=0;//continuous mode
    ECAP1.ECCTL2.TSCTRSTOP=1;//continuous mode

    ECAP1.ECEINT.CEVT2=1;//enable interrupt on event 2 (peripheral level)
    ECAP1.ECCLR.CEVT2=1;//clear interrupt on event 2

    EALLOW;
    INT4.ECAP1_INT=&cap1_isr;
    EDIS;

    PIEIER4.INTx1=1;//enable interrupt (PIE level)
    PIECTRL.ENPIE=1;//enable pie vector

    IER|=0x0008;
    EINT;
    ERTM;
}

interrupt void cap1_isr(){
    if(intCount<PERIOD_COUNT){
        period[intCount]=ECAP1.CAP2-ECAP1.CAP1;
    }
    intCount++;
    ECAP1.ECCLR.CEVT2=1;//clear interrupt on event 2
    ECAP1.ECCLR.CEVT1=1;//clear interrupt on event 2
    ECAP1.ECCLR.INT=1;//clear interrupt on event 2
    PIEACK|=0x0008;//clear pie interrupt
}
